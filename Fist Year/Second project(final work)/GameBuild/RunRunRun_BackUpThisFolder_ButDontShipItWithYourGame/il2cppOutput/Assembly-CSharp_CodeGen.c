﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void zombie::Start()
extern void zombie_Start_mFF542CD99C481A621069771D6EA91FB627A9C222 (void);
// 0x00000002 System.Void zombie::Update()
extern void zombie_Update_m7CB76E48DE6A6391794EA2A1C68B6D9794DB0FEB (void);
// 0x00000003 System.Void zombie::final()
extern void zombie_final_m8A4D5F0D242AEDA2D6E369DD8106E78D4603EDEF (void);
// 0x00000004 System.Void zombie::.ctor()
extern void zombie__ctor_m711FB116B67E80EB1C5A9BE7DBAD9012D9EABDC7 (void);
// 0x00000005 System.Void GestorAudio::Awake()
extern void GestorAudio_Awake_m77EA27845A6EB759915F886B5052D046B59CB8A5 (void);
// 0x00000006 System.Void GestorAudio::ReproducirSonido(System.String)
extern void GestorAudio_ReproducirSonido_m563E7F736510DAFB7CD4BC2903AA1B48AFF6D37B (void);
// 0x00000007 System.Void GestorAudio::PausarSonido(System.String)
extern void GestorAudio_PausarSonido_mE1598F76BED85347922F747D63C51B4C50F4A33B (void);
// 0x00000008 System.Void GestorAudio::.ctor()
extern void GestorAudio__ctor_m16485A82315DFCA5A273C212BC177060AB8B167B (void);
// 0x00000009 System.Void Menu::Jugar()
extern void Menu_Jugar_m671475320629921318FACFEE9DFAC12570189AFF (void);
// 0x0000000A System.Void Menu::Salir()
extern void Menu_Salir_mB1B272C802AB1586398225D3E4E6A898FA2C07FE (void);
// 0x0000000B System.Void Menu::.ctor()
extern void Menu__ctor_m87A18732D59A6382889C2771A372B622FD6CD58D (void);
// 0x0000000C System.Void Persistencia::Awake()
extern void Persistencia_Awake_m0EB5B929B8EFCDE6F68D669CCFE60E3EBE862F06 (void);
// 0x0000000D System.Void Persistencia::CargarData()
extern void Persistencia_CargarData_mA74EE704B9F16DBB62AD49EE3D97A6105840509C (void);
// 0x0000000E System.Void Persistencia::Update()
extern void Persistencia_Update_mC49287932928458789C74E96F64185B333196A29 (void);
// 0x0000000F System.Void Persistencia::GuardarData()
extern void Persistencia_GuardarData_m1796A8DA87076A2FB9478C43CFB77B2830FA081E (void);
// 0x00000010 System.Void Persistencia::.ctor()
extern void Persistencia__ctor_mA1D1F75664949EBBF4B65ED42F8FF67E788CD548 (void);
// 0x00000011 System.Void ActivarArmas::Start()
extern void ActivarArmas_Start_mA1568608DF585984953F828FC06DC2090659E0F4 (void);
// 0x00000012 System.Void ActivarArmas::OnTriggerEnter(UnityEngine.Collider)
extern void ActivarArmas_OnTriggerEnter_m8B39A3DF66B21B24E7FA4B886BF1D01CAB886624 (void);
// 0x00000013 System.Void ActivarArmas::.ctor()
extern void ActivarArmas__ctor_mCB71332D96C6B9C152CC8FFCD9AAED924296D9B2 (void);
// 0x00000014 System.Void AgarrarArmas::Activar(System.Int32)
extern void AgarrarArmas_Activar_mA03B7B12E06D333BB83E2C0EFC62347EACA5F303 (void);
// 0x00000015 System.Void AgarrarArmas::.ctor()
extern void AgarrarArmas__ctor_m10FD768FD4CB364A7BD604BCBCE6A3F675F6F6D4 (void);
// 0x00000016 System.Void MovimientoPlayer::Start()
extern void MovimientoPlayer_Start_m3524FAA739AFD989EC9B945B39B12ACD1DAC90CF (void);
// 0x00000017 System.Void MovimientoPlayer::OnCollisionStay()
extern void MovimientoPlayer_OnCollisionStay_mDD2738000A8354112BE65FD18032C91E1590F465 (void);
// 0x00000018 System.Void MovimientoPlayer::FixedUpdate()
extern void MovimientoPlayer_FixedUpdate_mDCFA6090779636A5448E5F77657455CE563A8B28 (void);
// 0x00000019 System.Void MovimientoPlayer::Update()
extern void MovimientoPlayer_Update_mDA37B43B022B3A56FDA356B7BCC1A256D1DEDEFF (void);
// 0x0000001A System.Void MovimientoPlayer::mecaigo()
extern void MovimientoPlayer_mecaigo_m3B9863F13BBA7EE9901718E3B50BC75F212F3350 (void);
// 0x0000001B System.Void MovimientoPlayer::nogolpeo()
extern void MovimientoPlayer_nogolpeo_mC1031317B71342F6BCA3571150F978AF01BC65F0 (void);
// 0x0000001C System.Void MovimientoPlayer::avanza()
extern void MovimientoPlayer_avanza_mFAF4DC487E974F61D787DC5DF615F8EE0C013EEF (void);
// 0x0000001D System.Void MovimientoPlayer::noavanza()
extern void MovimientoPlayer_noavanza_m01B2C53A63CF2CE56F086639EADD157280DF99CE (void);
// 0x0000001E System.Void MovimientoPlayer::parartiempo()
extern void MovimientoPlayer_parartiempo_mA3BED0650B377270AB53F403180A087B15F15F45 (void);
// 0x0000001F System.Void MovimientoPlayer::OnTriggerEnter(UnityEngine.Collider)
extern void MovimientoPlayer_OnTriggerEnter_m387FF54B32FFBA751A338EE99BCA31DA25D7F843 (void);
// 0x00000020 System.Void MovimientoPlayer::OnTriggerExit(UnityEngine.Collider)
extern void MovimientoPlayer_OnTriggerExit_mC61A9EC4B6B547506385FAAB2B020222C8AB1463 (void);
// 0x00000021 System.Void MovimientoPlayer::OnCollisionEnter(UnityEngine.Collision)
extern void MovimientoPlayer_OnCollisionEnter_mD9A87E5785FF620D799DE237FC431E419E4F74DA (void);
// 0x00000022 System.Void MovimientoPlayer::espadaactiva()
extern void MovimientoPlayer_espadaactiva_mE85602D8285A03FC6DC2D2EEBEDDCD9A8120ADD0 (void);
// 0x00000023 System.Void MovimientoPlayer::espadadesactiva()
extern void MovimientoPlayer_espadadesactiva_mCD2F02ECC0542E24B58C1C71B3E296D645A572D0 (void);
// 0x00000024 System.Void MovimientoPlayer::recibirDaUF1o()
extern void MovimientoPlayer_recibirDaUF1o_m35A9AC39C9753FE9A8BFB61B6BFA08563FF2FFC1 (void);
// 0x00000025 System.Void MovimientoPlayer::mostrarvida()
extern void MovimientoPlayer_mostrarvida_m11D11FFD019AF0530AABE7DBC17A3DF11FFEAD58 (void);
// 0x00000026 System.Void MovimientoPlayer::reinicio()
extern void MovimientoPlayer_reinicio_mF6FBB91F4BF6453431EAB75501FB5138447925AE (void);
// 0x00000027 System.Void MovimientoPlayer::gane()
extern void MovimientoPlayer_gane_mB9E696FD07539503FBFEC91AE9493A0269874C4D (void);
// 0x00000028 System.Void MovimientoPlayer::.ctor()
extern void MovimientoPlayer__ctor_m5EE533EAB2F798378026599C409C78482F984114 (void);
// 0x00000029 System.Void Salto::OnTriggerEnter(UnityEngine.Collider)
extern void Salto_OnTriggerEnter_m68E2F1E9A04B4D3054D11C055F879815B045F438 (void);
// 0x0000002A System.Void Salto::OnTriggerExit(UnityEngine.Collider)
extern void Salto_OnTriggerExit_m10CA920125DD993D9112254E8E6CBEA6914A17A2 (void);
// 0x0000002B System.Void Salto::.ctor()
extern void Salto__ctor_m975C300146B8EE568ADB3279E8F9F9D6CC5A6EE3 (void);
// 0x0000002C System.Void dash1::Start()
extern void dash1_Start_m5A5010AE0C5F286BDA25E3D478B6F4E7CAE0F091 (void);
// 0x0000002D System.Void dash1::Update()
extern void dash1_Update_m5057B733D0E9213606B285E1502C93291B01EEEB (void);
// 0x0000002E System.Void dash1::FixedUpdate()
extern void dash1_FixedUpdate_mE3DBDE4EDA58B022943539632513405FDE4B1CBA (void);
// 0x0000002F System.Void dash1::dashh()
extern void dash1_dashh_mE014A0A17D1E0F688597DF1FC66998EA54974468 (void);
// 0x00000030 System.Void dash1::.ctor()
extern void dash1__ctor_mC0C167E3459FC53B29311F84E24E31DA320B5C9C (void);
// 0x00000031 System.Void Sonido::.ctor()
extern void Sonido__ctor_m3CE32405ED71E91B6E0DB965D8E5F2A9748A9D5E (void);
// 0x00000032 System.Void disparos::Start()
extern void disparos_Start_m351516F71F08766294610A4689B6DFB6F8680D7B (void);
// 0x00000033 System.Void disparos::Update()
extern void disparos_Update_m249D75724866732B1A7FC93BBC21BBB162D16CB2 (void);
// 0x00000034 System.Collections.IEnumerator disparos::Esperar()
extern void disparos_Esperar_mD599B92659E2C6A8250C32EA2D626CDFA37CEAE9 (void);
// 0x00000035 System.Void disparos::OnCollisionEnter(UnityEngine.Collision)
extern void disparos_OnCollisionEnter_mE98C48EDFF3F1E792575EB5EEC3E01B755151BE4 (void);
// 0x00000036 System.Void disparos::.ctor()
extern void disparos__ctor_m71D181270B5316BC44882E83B6FC707E527595B0 (void);
// 0x00000037 System.Void espada::Start()
extern void espada_Start_mDD41E4403826AD9B76CDA83E61752C1FBB09D713 (void);
// 0x00000038 System.Void espada::OnTriggerEnter(UnityEngine.Collider)
extern void espada_OnTriggerEnter_m217AD753954CEA8C3CFC11EC490AA6F44EE36B92 (void);
// 0x00000039 System.Void espada::seactiva()
extern void espada_seactiva_m07FD14B26CF81D88DF38845699B1DAC8B156CAA8 (void);
// 0x0000003A System.Void espada::desactiva()
extern void espada_desactiva_m233B74E754713241FE8011D2682B42030A301CBC (void);
// 0x0000003B System.Void espada::.ctor()
extern void espada__ctor_m88EF6A12A1079AB256AD108C9FAAD28B9ED2777E (void);
// 0x0000003C System.Void pared::explotar(System.Int32)
extern void pared_explotar_mC0FE108AF92F7E792C70B27981A3A44AB42D65F3 (void);
// 0x0000003D System.Void pared::MostrarExplosion()
extern void pared_MostrarExplosion_mC76D6004AC56CF1685069122BFFA138C70FA7430 (void);
// 0x0000003E System.Void pared::.ctor()
extern void pared__ctor_m7F7D8FE17F3075F9A204C69F680BDB3F0AC53F75 (void);
// 0x0000003F System.Void text::Start()
extern void text_Start_m9261498682354AF00DD4F215EE141988298CB07B (void);
// 0x00000040 System.Void text::OnTriggerEnter(UnityEngine.Collider)
extern void text_OnTriggerEnter_m20F359EFB73D649F3E5AFB384106980119402F42 (void);
// 0x00000041 System.Void text::OnTriggerExit(UnityEngine.Collider)
extern void text_OnTriggerExit_m7382655447EEC4FC895FE1C31CFBE93A8E7E95DB (void);
// 0x00000042 System.Void text::.ctor()
extern void text__ctor_mCF6853B3EB7C4C59F424380986A6D884217B68D1 (void);
// 0x00000043 System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x00000044 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8 (void);
// 0x00000045 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47 (void);
// 0x00000046 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F (void);
// 0x00000047 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E (void);
// 0x00000048 System.Void GestorAudio_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m271C5A332BFEF7E93A0559DFA2D1D244AD8F5361 (void);
// 0x00000049 System.Boolean GestorAudio_<>c__DisplayClass3_0::<ReproducirSonido>b__0(Sonido)
extern void U3CU3Ec__DisplayClass3_0_U3CReproducirSonidoU3Eb__0_mF29B1606635344A2673DC7CDB631F8DE13CE6D9E (void);
// 0x0000004A System.Void GestorAudio_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m7F541BF60103725462F432B3E5E0B5AE886D949E (void);
// 0x0000004B System.Boolean GestorAudio_<>c__DisplayClass4_0::<PausarSonido>b__0(Sonido)
extern void U3CU3Ec__DisplayClass4_0_U3CPausarSonidoU3Eb__0_m5622BAD37598464FE89E9A31F13D444AEE2F49F1 (void);
// 0x0000004C System.Void Persistencia_DataPercistencia::.ctor()
extern void DataPercistencia__ctor_m64A5E5331ADCAEB10728880C39C998A27DFFC40C (void);
// 0x0000004D System.Void Persistencia_Punto::.ctor(UnityEngine.Vector3)
extern void Punto__ctor_m7393D85BF97D95205FE750C8B166A564D876B0C4 (void);
// 0x0000004E UnityEngine.Vector3 Persistencia_Punto::aVector()
extern void Punto_aVector_m6DCD4EE102E079CC32A77E81F6A2A1903808D988 (void);
// 0x0000004F System.Void disparos_<Esperar>d__7::.ctor(System.Int32)
extern void U3CEsperarU3Ed__7__ctor_mFB88038F78035DC84267397AC3F77655F021ED42 (void);
// 0x00000050 System.Void disparos_<Esperar>d__7::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__7_System_IDisposable_Dispose_mE0F236C15CCE307A6F9C052FAA2FE2FF589AE976 (void);
// 0x00000051 System.Boolean disparos_<Esperar>d__7::MoveNext()
extern void U3CEsperarU3Ed__7_MoveNext_m31BB1EE67C8ECB9520ED3E803374253683CAC0A0 (void);
// 0x00000052 System.Object disparos_<Esperar>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B85384BF0EE9A3FE3E01A301827C6332C3EB709 (void);
// 0x00000053 System.Void disparos_<Esperar>d__7::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__7_System_Collections_IEnumerator_Reset_m9472296A70BD71E4753F096CBA85891C208403B5 (void);
// 0x00000054 System.Object disparos_<Esperar>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__7_System_Collections_IEnumerator_get_Current_mBAED8A309D91ECFEDB0CF8DE9F96E3D7D679E95B (void);
// 0x00000055 System.Void Readme_Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
// 0x00000056 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E (void);
// 0x00000057 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247 (void);
// 0x00000058 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController_CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282 (void);
// 0x00000059 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623 (void);
// 0x0000005A System.Void UnityTemplateProjects.SimpleCameraController_CameraState::.ctor()
extern void CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322 (void);
static Il2CppMethodPointer s_methodPointers[90] = 
{
	zombie_Start_mFF542CD99C481A621069771D6EA91FB627A9C222,
	zombie_Update_m7CB76E48DE6A6391794EA2A1C68B6D9794DB0FEB,
	zombie_final_m8A4D5F0D242AEDA2D6E369DD8106E78D4603EDEF,
	zombie__ctor_m711FB116B67E80EB1C5A9BE7DBAD9012D9EABDC7,
	GestorAudio_Awake_m77EA27845A6EB759915F886B5052D046B59CB8A5,
	GestorAudio_ReproducirSonido_m563E7F736510DAFB7CD4BC2903AA1B48AFF6D37B,
	GestorAudio_PausarSonido_mE1598F76BED85347922F747D63C51B4C50F4A33B,
	GestorAudio__ctor_m16485A82315DFCA5A273C212BC177060AB8B167B,
	Menu_Jugar_m671475320629921318FACFEE9DFAC12570189AFF,
	Menu_Salir_mB1B272C802AB1586398225D3E4E6A898FA2C07FE,
	Menu__ctor_m87A18732D59A6382889C2771A372B622FD6CD58D,
	Persistencia_Awake_m0EB5B929B8EFCDE6F68D669CCFE60E3EBE862F06,
	Persistencia_CargarData_mA74EE704B9F16DBB62AD49EE3D97A6105840509C,
	Persistencia_Update_mC49287932928458789C74E96F64185B333196A29,
	Persistencia_GuardarData_m1796A8DA87076A2FB9478C43CFB77B2830FA081E,
	Persistencia__ctor_mA1D1F75664949EBBF4B65ED42F8FF67E788CD548,
	ActivarArmas_Start_mA1568608DF585984953F828FC06DC2090659E0F4,
	ActivarArmas_OnTriggerEnter_m8B39A3DF66B21B24E7FA4B886BF1D01CAB886624,
	ActivarArmas__ctor_mCB71332D96C6B9C152CC8FFCD9AAED924296D9B2,
	AgarrarArmas_Activar_mA03B7B12E06D333BB83E2C0EFC62347EACA5F303,
	AgarrarArmas__ctor_m10FD768FD4CB364A7BD604BCBCE6A3F675F6F6D4,
	MovimientoPlayer_Start_m3524FAA739AFD989EC9B945B39B12ACD1DAC90CF,
	MovimientoPlayer_OnCollisionStay_mDD2738000A8354112BE65FD18032C91E1590F465,
	MovimientoPlayer_FixedUpdate_mDCFA6090779636A5448E5F77657455CE563A8B28,
	MovimientoPlayer_Update_mDA37B43B022B3A56FDA356B7BCC1A256D1DEDEFF,
	MovimientoPlayer_mecaigo_m3B9863F13BBA7EE9901718E3B50BC75F212F3350,
	MovimientoPlayer_nogolpeo_mC1031317B71342F6BCA3571150F978AF01BC65F0,
	MovimientoPlayer_avanza_mFAF4DC487E974F61D787DC5DF615F8EE0C013EEF,
	MovimientoPlayer_noavanza_m01B2C53A63CF2CE56F086639EADD157280DF99CE,
	MovimientoPlayer_parartiempo_mA3BED0650B377270AB53F403180A087B15F15F45,
	MovimientoPlayer_OnTriggerEnter_m387FF54B32FFBA751A338EE99BCA31DA25D7F843,
	MovimientoPlayer_OnTriggerExit_mC61A9EC4B6B547506385FAAB2B020222C8AB1463,
	MovimientoPlayer_OnCollisionEnter_mD9A87E5785FF620D799DE237FC431E419E4F74DA,
	MovimientoPlayer_espadaactiva_mE85602D8285A03FC6DC2D2EEBEDDCD9A8120ADD0,
	MovimientoPlayer_espadadesactiva_mCD2F02ECC0542E24B58C1C71B3E296D645A572D0,
	MovimientoPlayer_recibirDaUF1o_m35A9AC39C9753FE9A8BFB61B6BFA08563FF2FFC1,
	MovimientoPlayer_mostrarvida_m11D11FFD019AF0530AABE7DBC17A3DF11FFEAD58,
	MovimientoPlayer_reinicio_mF6FBB91F4BF6453431EAB75501FB5138447925AE,
	MovimientoPlayer_gane_mB9E696FD07539503FBFEC91AE9493A0269874C4D,
	MovimientoPlayer__ctor_m5EE533EAB2F798378026599C409C78482F984114,
	Salto_OnTriggerEnter_m68E2F1E9A04B4D3054D11C055F879815B045F438,
	Salto_OnTriggerExit_m10CA920125DD993D9112254E8E6CBEA6914A17A2,
	Salto__ctor_m975C300146B8EE568ADB3279E8F9F9D6CC5A6EE3,
	dash1_Start_m5A5010AE0C5F286BDA25E3D478B6F4E7CAE0F091,
	dash1_Update_m5057B733D0E9213606B285E1502C93291B01EEEB,
	dash1_FixedUpdate_mE3DBDE4EDA58B022943539632513405FDE4B1CBA,
	dash1_dashh_mE014A0A17D1E0F688597DF1FC66998EA54974468,
	dash1__ctor_mC0C167E3459FC53B29311F84E24E31DA320B5C9C,
	Sonido__ctor_m3CE32405ED71E91B6E0DB965D8E5F2A9748A9D5E,
	disparos_Start_m351516F71F08766294610A4689B6DFB6F8680D7B,
	disparos_Update_m249D75724866732B1A7FC93BBC21BBB162D16CB2,
	disparos_Esperar_mD599B92659E2C6A8250C32EA2D626CDFA37CEAE9,
	disparos_OnCollisionEnter_mE98C48EDFF3F1E792575EB5EEC3E01B755151BE4,
	disparos__ctor_m71D181270B5316BC44882E83B6FC707E527595B0,
	espada_Start_mDD41E4403826AD9B76CDA83E61752C1FBB09D713,
	espada_OnTriggerEnter_m217AD753954CEA8C3CFC11EC490AA6F44EE36B92,
	espada_seactiva_m07FD14B26CF81D88DF38845699B1DAC8B156CAA8,
	espada_desactiva_m233B74E754713241FE8011D2682B42030A301CBC,
	espada__ctor_m88EF6A12A1079AB256AD108C9FAAD28B9ED2777E,
	pared_explotar_mC0FE108AF92F7E792C70B27981A3A44AB42D65F3,
	pared_MostrarExplosion_mC76D6004AC56CF1685069122BFFA138C70FA7430,
	pared__ctor_m7F7D8FE17F3075F9A204C69F680BDB3F0AC53F75,
	text_Start_m9261498682354AF00DD4F215EE141988298CB07B,
	text_OnTriggerEnter_m20F359EFB73D649F3E5AFB384106980119402F42,
	text_OnTriggerExit_m7382655447EEC4FC895FE1C31CFBE93A8E7E95DB,
	text__ctor_mCF6853B3EB7C4C59F424380986A6D884217B68D1,
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8,
	SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47,
	SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F,
	SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E,
	U3CU3Ec__DisplayClass3_0__ctor_m271C5A332BFEF7E93A0559DFA2D1D244AD8F5361,
	U3CU3Ec__DisplayClass3_0_U3CReproducirSonidoU3Eb__0_mF29B1606635344A2673DC7CDB631F8DE13CE6D9E,
	U3CU3Ec__DisplayClass4_0__ctor_m7F541BF60103725462F432B3E5E0B5AE886D949E,
	U3CU3Ec__DisplayClass4_0_U3CPausarSonidoU3Eb__0_m5622BAD37598464FE89E9A31F13D444AEE2F49F1,
	DataPercistencia__ctor_m64A5E5331ADCAEB10728880C39C998A27DFFC40C,
	Punto__ctor_m7393D85BF97D95205FE750C8B166A564D876B0C4,
	Punto_aVector_m6DCD4EE102E079CC32A77E81F6A2A1903808D988,
	U3CEsperarU3Ed__7__ctor_mFB88038F78035DC84267397AC3F77655F021ED42,
	U3CEsperarU3Ed__7_System_IDisposable_Dispose_mE0F236C15CCE307A6F9C052FAA2FE2FF589AE976,
	U3CEsperarU3Ed__7_MoveNext_m31BB1EE67C8ECB9520ED3E803374253683CAC0A0,
	U3CEsperarU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B85384BF0EE9A3FE3E01A301827C6332C3EB709,
	U3CEsperarU3Ed__7_System_Collections_IEnumerator_Reset_m9472296A70BD71E4753F096CBA85891C208403B5,
	U3CEsperarU3Ed__7_System_Collections_IEnumerator_get_Current_mBAED8A309D91ECFEDB0CF8DE9F96E3D7D679E95B,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
	CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E,
	CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247,
	CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282,
	CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623,
	CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322,
};
static const int32_t s_InvokerIndices[90] = 
{
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	1133,
	23,
	23,
	23,
	9,
	23,
	9,
	23,
	1134,
	1133,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	26,
	1134,
	1321,
	26,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	90,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
